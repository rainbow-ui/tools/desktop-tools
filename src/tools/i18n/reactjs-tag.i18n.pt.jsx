module.exports = {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~creditcard~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    and: "-",
    month: 'Mês',
    year: 'Ano',
    backNo: 'Últimos três',
    creditCard: "Cartão de crédito",
    effectiveDate: "Data efetiva",
    regexp: 'O primeiro código é 2 ou 3 ou 4 ou 5',
    regexp1: 'Quando o primeiro código é 3, o segundo código só pode ser 5 ou 7',
    regexp2: 'O comprimento é 15',
    regexp3: 'O comprimento é 16',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~cropper-upload~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    cropperCustomMessage: "personalização",
    imgTooLarge: "A imagem é muito grande, selecione novamente!",
    imgNotMatch: "A imagem não corresponde, selecione novamente!" ,
    formatNotMatch: "Desculpe, o formato de entrada não corresponde.",
    proportionSelection: "Seleção de proporção",
    enterCustomScale: "Insira a escala personalizada",
    reUpload: "reenviar",
    startCutting: "Comece a cortar",
    usePhotos: "Usar fotos",
    preview:"Antevisão",

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~datetimepicker~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    formatSplitBothIsEmpty: 'O propType formatSplit ou formatDateSplit está vazio.',
    formatSplitIsEmpty: 'O propType formatSplit está vazio.',
    formatMsg: 'Por favor, insira o formato de data correto',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-address~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    searchError: 'Sem dados relevantes.',
    addressLabelOne: 'Código Postal',
    addressLabelTwo: 'City',
    addressLabelThree: 'Endereço',
    addressLabelFour: 'AddressKana',
    search: 'Pesquisar',
    reset: 'Redefinir',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-finance~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    labelOne: 'Código bancario',
    labelTwo: 'Nombre de la sucursal',
    labelThree: 'Código de sucursal postal',
    labelFour: 'Depósitos',
    labelFive: 'Cuenta bancaria JP',
    labelSix: 'Cuenta de publicación',
    labelSeven: 'Nombre de cuenta',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-name~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    JPInputValidator: {
        jpNameRequiredMessage: "O campo é obrigatório.",
        jpNameLengthMessage: "O valor deve ter mais de {0} e menos de {1} caracteres",
        jpNameMinLengthMessage: "O valor deve ter mais de {0} caracteres",
        jpNameMaxLengthMessage: "O valor deve ter menos de {0} caracteres",
        jpNameWholeFullTextMessage: 'Por favor, insira caracteres de byte duplo',
        jpNameExistsFullSpaceMessage: 'Separe o sobrenome e o nome com um espaço de byte duplo.'
    },

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-name-kanji~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    jpNameKanjiRequiredMessage: '',
    wholeFullTextMessage: '',
    existsFullSpaceMessage: '',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-name-kana~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tw-fixed-phone~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    fixedPhone: 'Telefone fixo',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tw-individual-id~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    idCard: 'tarjeta de identificación',
    twIndividualStringLength: "La longitud del campo debe ser de 10 dígitos",
    twIndividualRegexp: '(La primera letra es mayúscula, el resto son números)',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tw-resident-permit~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    residentPermit: 'Permiso de residencia',
    stringLength: "La longitud del campo debe ser de 10 dígitos",
    twResidentRegexp: '(Los primeros 2 dígitos son letras mayúsculas, los últimos 8 dígitos son números y la letra del segundo dígito representa el género 【A, C, E masculino, B, D, F femenino】)',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~core~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    expandMessage: "mais",
    closeMessage: "guarda",
    allMessage: "ilimitada",
    customMessage: "personalização",
    expandRowNumMessage: "O número de condições de expansão por defeito deve ser um número inteiro maior que 0",
    filterMoreNotSupportMessage: "O plugin de consulta fiterMore não suporta o método {0}",
    confirmMessage: "confirmação",
    multiselectMessage: "Escolha múltipla",
    singleSelectMessage: "rádio",
    Selected: "Já foi escolhido.",
    UnSelected: "Não seleccionado",
    AddAll: "Adicionar todos",
    Add: "adicionar",
    RemoveAll: "Remover todos",
    Remove: "Remover",
    Data: {
        All: "Todos os",
    },
    All: "Todos",
    Common: "comum",
    // DataTable
    DataTable: {
        SelectAll: "Escolha múltipla",
        SingleSelect: "rádio",
        NoResult: "Não há registo."
    },

    // AdvCascade
    PropertyError: 'Valor errado para a ligação à Propriedade',

    // Pagination
    DropDownInfo: "Cada página {0}",
    Search: "Pesquisar",
    FirstPage: "Ficha técnica",
    PreviousPage: "Página anterior",
    NextPage: "Página seguinte",
    LastPage: "página",
    Pages: "páginas",
    Total: "Total",
    Items: "Itens",


    // Input
    // Select
    BlankOption: "Por favor, escolha",

    // DateRangePicker
    DateRangePicker: {
        ApplyLabel: 'A certeza',
        CancelLabel: 'Cancelar',
        FromLabel: 'Hora de início',
        ToLabel: 'Hora final',
        CustomRangeLabel: 'personalização',
        times: ["Horas", "Minutos", "Segundos"],
        timeChoose: 'Escolha do tempo',
        BackDate: "Data de regress",
        Clear: "Esvaziar",
        Today: "Hoje",
        Close: "Encerramento",
        DaysOfWeek: ['DOM', 'SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SAB'],
        MonthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
    },

    // TwoText
    TwoText: {
        dialogTitle: "Pesquisa Code Table",
        tableHeaderTitle: "Pesquisa Code Table",
        keyColumn: "Botão",
        valueColumn: "Valores",
        confirmButton: "Confirmação",
        cancelButton: "Cancelar",
        error: "Por favor selecione um registro",
    },

    // Data
    // FullCalendar
    FullCalendar: {
        MonthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        MonthNamesShort: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
        DayNames: ['DOM', 'SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SAB'],
        DayNamesShort: ['DOM', 'SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SAB'],
        Today: ["Hoje"],
        AM: "De manhã",
        PM: "À tarde",
        FirstDay: 1,
        ButtonText: {
            //Prev: "<",
            //Next: ">",
            Prev: "No mês passado",
            Next: "No mês que vem",
            Today: "Este mês",
            Month: "mês",
            Week: "Semanas",
            Day: "De outubro"
        }
    },

    // Integration
    // DropZone
    DropZone: {
        dictDefaultMessage: "Arraste o arquivo para lá (ou clique aqui)",
        dictFallbackMessage: "O seu navegador não suporta componentes de upload.",
        dictFallbackText: "Por favor, use o formulário de retorno para carregar os seus arquivos como nos dias de olden.",
        dictInvalidFileType: "Você não pode upload esse tipo de arquivo, o tipo de arquivo não é suportado.",
        dictFileTooBig: "O arquivo é muito grande({{filesize}}MB). Suporte máximo para o upload de arquivos: {{maxFilesize}}MB.",
        dictResponseError: "O carregamento do ficheiro não foi feito!",
        dictCancelUpload: "Cancelar o upload",
        dictCancelUploadConfirmation: "Tens a certeza que queres cancelar o upload?",
        dictRemoveFile: "Remover arquivos",
        dictMaxFilesExceeded: "Você pode carregar até um máximo de {{maxFiles}} arquivos de cada vez",
    },

    // FileUpload
    UploadSuccess: 'Sucesso no upload!',
    RemoveSuccess: 'Sucesso removido!',

    MSG_NOT_FOUND: "Informação não encontrada",
    MSG_REGULAR_EXPRESSION_ERROR: "Só podem ser introduzidos números.",
    MSG_DATACONTEXT_KEY_DUPLICATE: "(DataContext key)Repito, exame emocional.",

    //validator message
    InputValidator: {
        RequiredMessage: "O Campo é necessário",
        LengthMessage: "O comprimento do campo deve ser maior que {0} e menor que {1}",
        MinLengthMessage: "O comprimento do campo deve ser maior que {0}",
        MaxLengthMessage: "O comprimento do campo deve ser inferior a {0}",
    },
    DigitValidator: {
        ValueMessage: "O valor deve ser maior que {0}, e menor que {1}",
        MinValueMessage: "O valor deve ser maior que {0}",
        MaxValueMessage: "O valor deve ser inferior a {0}",
    },
    EmailValidator: "Endereço de e-mail não é legal",
    Page: {
        needContinue: "Se a sessão está prestes a esgotar o tempo para continuar ou não a operação?",
        messageFront: "O sistema será",
        messageEnd: "Saída em segundos.",
        confirm: "é"
    },
    Required: "Opções obrigatórias",
    DateTimePicker: {
        maxDateErrorMessage: "A data de entrada excede a data máxima:",
        minDateErrorMessage: "A data de entrada é posterior à data mínima:"
    },
    // UISearch
    Searching: 'Em busca...',

    //TimeLine
    Online: 'Conectados',
    Offline: 'desligada',

    // UILink
    noLinkTitle: 'Sem título',

    // popConfirm 
    PopConfirmTitle: 'Tens a certeza?',

    //sideMenu
    Overview: 'Todo o menu',
    MyFavorites: 'minha coleção',
    MenuNoResults: 'Resultado da consulta não encontrado',
    MenuRecently: 'recente:',
    SearchPlaceHolder: 'Menu de pesquisa'

};
