module.exports = {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~creditcard~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    and: "—",
    month: 'Tháng',
    year:'Năm',
    backNo:'Ba ký tự cuối',
    creditCard: "Thẻ Tín dụng",
    effectiveDate: "Ngày hiệu lực",
    regexp: 'Số đầu tiên là 2 hoặc 3 hoặc 4 hoặc 5',
    regexp1: 'Khi số đầu tiên là 3, thì số thứ hai chỉ có thể là 5 hoặc 7',
    regexp2: 'Độ dài là 15',
    regexp3: 'Độ dài là 16',
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~cropper-upload~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    cropperCustomMessage: "Tùy Chỉnh",
    imgTooLarge: "Hình quá lơn, vui lòng chọn lại!",
    imgNotMatch: "Hình không đúng, vui lòng chọn lại!",
    formatNotMatch: "Xin lỗi, định dạng nhập không đúng.",
    proportionSelection: "Chọn tỉ lệ",
    enterCustomScale: "Nhập tỉ lệ tùy chỉnh",
    reUpload: "Tải lên lại",
    startCutting: "Bắt đầu cắt",
    usePhotos: "Sử dụng hình ảnh",
    preview:"Xem trước",

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~datetimepicker~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    formatSplitBothIsEmpty: 'Phân chia định dạng hoặc phân chia định dạng ngày bị trống.',
    formatSplitIsEmpty: 'Phân chia định dạng bị trống.',
    formatMsg:'Xin nhập đúng định dạng ngày',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-address~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    searchError: 'Không tìm thấy dữ liệu lien quan.',
    addressLabelOne: 'Mã bưu chính',
    addressLabelTwo: 'Thành phố',
    addressLabelThree : 'Địa chỉ',
    addressLabelFour: 'Địa chỉ Kana',
    search: 'Tìm kiếm',
    reset: 'Cài lại',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-finance~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    labelOne: 'Mã ngân hàng',
    labelTwo: 'Tên chi nhánh',
    labelThree : 'Tên chi nhánh',
    labelFour: 'Ký gửi',
    labelFive: 'Tài khoản ngân hàng JP',
    labelSix: 'Tài khoản ngân hàng JP',
    labelSeven: 'Tên tài khoản',    

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-name~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    JPInputValidator: {
        jpNameRequiredMessage: "Trường thông tin yêu cầu.",
        jpNameLengthMessage: "Giá trị phải lớn hơn {0} và nhỏ hơn {1} ký tự",
        jpNameMinLengthMessage: "Giá trị phải lớn hơn {0} ký tự",
        jpNameMaxLengthMessage: "Giá trị phải nhỏ hơn {0} ký tự",
        jpNameWholeFullTextMessage: 'Xin nhập ký tự ở định dạng double-byte',
        jpNameExistsFullSpaceMessage: 'Chia họ và tên với khoảng trắng định dạng double-byte.'
    },

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-name-kanji~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    jpNameKanjiRequiredMessage: '',
    wholeFullTextMessage: '',
    existsFullSpaceMessage: '',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-name-kana~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tw-fixed-phone~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    fixedPhone: 'Điện thoại cố định',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tw-individual-id~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    idCard: 'ID card',
    twIndividualStringLength: "Độ dài thông tin phải lớn hơn 10",
    twIndividualRegexp: '(Ký tự đầu tiên phải là chữ hoa, còn lại là số)',
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tw-resident-permit~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    residentPermit: 'Quyền cư trú',
    stringLength: "Độ dài thông tin phải lớn hơn 10",
    twResidentRegexp: '(Hai ký tự đầu tiên phải là chữ hoa, 8 ký tự cuối là số, và ký tự chữ thứ nhì đại diện cho giới tính 【A, C, E Nam , B, D, F Nữ】)',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~core~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    expandMessage: "Hơn nữa",
    closeMessage: "Xếp lại",
    allMessage: "Không giới hạn",
    customMessage: "Tùy chỉnh",
    expandRowNumMessage: "Giá trị mặc định của hàng mở rộng phải lớn hơn 0",
    filterMoreNotSupportMessage: "Sự tích hợp của Lọc Mở Rộng không hỗ trợ chức năng của {0}",
    confirmMessage: "Xác nhận",
    multiselectMessage: "Chọn nhiều",
    singleSelectMessage: "Chọn một",
    Selected:"Được chọn",
    UnSelected: "Bỏ chọn",
    AddAll: "Thêm tất cả",
    Add: "Thêm",
    RemoveAll:"Loaị bỏ tất cả",
    Remove: "Loại bỏ",
    Data: {
        All: "Tất cả",
    },
    All: "All",
    Common: "Thông thường",
    // DataTable
    DataTable: {
        SelectAll: "Tất cả",
        SingleSelect: "Một",
        NoResult:"Không có kết quả."
    },

    // Pagination
    DropDownInfo: "Từng trang{0}",
    Search: "Tìm kiếm",
    FirstPage: "Trang đầu",
    PreviousPage: "Trang trước",
    NextPage: "Trang kế",
    LastPage: "Trang kế",
    Pages: "Trang",

    // Input
    // Select
    BlankOption: "Xin chọn",

    // DateRangePicker
    DateRangePicker: {
        ApplyLabel: "Xác nhận",
        CancelLabel: "Hủy",
        FromLabel: "Bắt đầu",
        ToLabel: "Kết thúc",
        CustomRangeLabel: "Tùy chỉnh",
        times: ["Giờ", "Phút", "Giây"],
        timeChoose: 'Chọn thời gian',
        BackDate: "Chọn thời gian",
        Clear: "Rõ ràng",
        Today: "Hôm nay",
        Close: "Đóng",
        DaysOfWeek: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
        MonthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
    },

    // TwoText
    TwoText: {
        dialogTitle: "Bảng tìm mã",
        tableHeaderTitle: "Bảng tìm mã",
        keyColumn: "Khóa",
        valueColumn: "Giá trị",
        confirmButton: "Giá trị",
        cancelButton: "Hủy",
        error: "Xin chọn ít nhất 1 giá trị",
    },

    // Data
    // FullCalendar
    FullCalendar: {
        MonthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
        MonthNamesShort: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
        DayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
        DayNamesShort: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
        Today: ["Hôm nay"],
        FirstDay: 1,
        AM:"am",
        PM:"pm",
        ButtonText: {
            //Prev: "<",
            //Next: ">",
            Prev: "Tháng trước",
            Next: "Tháng sau",
            Today: "Hôm nay",
            Month: "Tháng sau",
            Week: "Tuần",
            Day: "Ngày"
        }
    },

    // Integration
    // DropZone
    DropZone: {
        dictDefaultMessage: "Bỏ tập tin hoặc nhấp vào đây để tải lên",
        dictFallbackMessage: "Trình duyệt của bạn không hỗ trợ kéo thả để tải tập tin.",
        dictFallbackText: "Vui lòng sử dụng mẫu dự phòng phía dưới để tải tập tin lên như trước đây.",
        dictInvalidFileType: "Bạn không thể tải lên loại tập tin này, loại tập tin này chưa được hỗ trợ.",
        dictFileTooBig: "Dung lượng tập tin quá lớn ({{filesize}}MB). Dung lượng tối đa được hỗ trợ là: {{maxFilesize}}MB.",
        dictResponseError: "Tải tập tin lên thất bại!",
        dictCancelUpload: "Hủy tập tin",
        dictCancelUploadConfirmation: "Bạn có chắc muốn hủy tải lên không?",
        dictRemoveFile: "Xóa tập tin",
        dictMaxFilesExceeded: "Bạn chỉ có thể tải lên số lượng tối đa là {{maxFiles}} tập tin",
    },

    MSG_NOT_FOUND: "Không tìm thấy thông báo",
    MSG_REGULAR_EXPRESSION_ERROR: "Bạn chỉ có thể điền số",
    MSG_DATACONTEXT_KEY_DUPLICATE: "(Khóa dữ liệu) bị trùng, xin kiểm tra.",

    //validator message
    InputValidator: {
      RequiredMessage: "Trường thông tin yêu cầu.",
      LengthMessage: "Giá trị phải lớn hơn {0} và nhỏ hơn {1} ký tự",
      MinLengthMessage: "Giá trị phải lớn hơn {0} ký tự",
      MaxLengthMessage: "Giá trị phải nhỏ hơn {0} ký tự",
    },
    DigitValidator: {
      ValueMessage: "Giá trị phải lớn hơn {0} và nhỏ hơn {1}",
      MinValueMessage: "Giá trị phải lớn hơn {0}",
      MaxValueMessage: "Giá trị phải lớn hơn nhỏ hơn {0}",
    },
    EmailValidator: "Không đúng định dạng email",
    Page: {
        needContinue: "Cần tiêp tục?",
        messageFront: "Đăng xuất sau đó ",
        messageEnd:" Giây.",
        confirm:"Xác nhận"
    },
    Required:"Bắt buộc.",
    DateTimePicker:{
        maxDateErrorMessage: "Ngày nhập lớn hơn ngày tối đa:",
        minDateErrorMessage: "Ngày nhập nhỏ hơn ngày tối thiểu:"
    },
    // UISearch
    Searching: 'Đang tìm kiếm...',
    //error message
    EffectiveDateError: 'Xin nhập ngày có hiệu lực.',
    DateFormat: "Xin nhập đúng định dạng ngày.",

    //TimeLine
    Online: 'Trực tuyến',
    Offline: 'Ngoại tuyến',

    //sideMenu
    Overview: 'Tổng quan',
    MyFavorites: 'Mục ưa thích',
    MenuNoResults: 'Không tìm thấy kết quả nào',
    MenuRecently: 'Gần đây:',
    SearchPlaceHolder: 'Tìm kiếm'

};
