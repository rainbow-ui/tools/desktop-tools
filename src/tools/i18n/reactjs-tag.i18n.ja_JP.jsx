﻿module.exports = {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~creditcard~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    and: "-",
    month: '月',
    year:'年',
    backNo:'ラストスリー',
    creditCard:'クレジットカード',
    effectiveDate:'発効日',
    regexp: '最初のコードは2または3または4または5',
    regexp1: '最初のコードが3の場合,2番目のコードは5または7のみになります',
    regexp2: '長さは15',
    regexp3: '長さは16',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~cropper-upload~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    cropperCustomMessage: "カスタマイズ",
    imgTooLarge: "写真が大きすぎますので,もう一度お選びください！",
    imgNotMatch: "写真が一致しません。もう一度選択してください。" ,
    formatNotMatch: "申し訳ありませんが,入力形式が一致しません。",
    proportionSelection: "比率の選択",
    enterCustomScale: "カスタムスケールを入力してください",
    reUpload: "再アップロード",
    startCutting: "カットを開始します",
    usePhotos: "写真を使う",
    preview:"プレビュー",

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~datetimepicker~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    formatSplitBothIsEmpty: 'propTypeformatSplitまたはformatDateSplitは空です。',
    formatSplitIsEmpty: 'propType formatSplitは空です。',
    formatMsg: '正しい日付形式を入力してください',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-address~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    searchError: '関連データなし。',
    addressLabelOne: '郵便番号',
    addressLabelTwo: '都道府县',
    addressLabelThree : '住所（漢字）',
    addressLabelFour: '住所（カナ）',
    search: '検索',
    reset: 'クリア',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-finance~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    labelOne: '金融機関',
    labelTwo: '支店',
    labelThree : '通帳記号',
    labelFour: '預金種目',
    labelFive: '口座番号',
    labelSix: '通帳番号',
    labelSeven: '口座名義人（カナ）',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-name~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    JPInputValidator: {
        jpNameRequiredMessage: "必須項目を入力してください",
        jpNameLengthMessage: "{0}値から,より多くでよりよい{1}よりもなければ",
        jpNameMinLengthMessage: "値が少ないからである{0}よりもなければ",
        jpNameMaxLengthMessage: "値が{1}よりも少ないからでなければなりません",
        jpNameWholeFullTextMessage: 'は全角文字で入力してください',
        jpNameExistsFullSpaceMessage: '名字と名前の間は全角スペースで区切ってください'
    },

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-name-kanji~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    jpNameKanjiRequiredMessage: '',
    wholeFullTextMessage: '',
    existsFullSpaceMessage: '',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-name-kana~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tw-fixed-phone~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    fixedPhone: '固定電話',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tw-individual-id~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    idCard: 'IDカード',
    twIndividualStringLength: "フィールドの長さは10桁である必要があります",
    twIndividualRegexp: '（最初の文字は大文字,残りは数字）',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tw-resident-permit~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    residentPermit: '居住許可',
    stringLength: "フィールドの長さは10桁である必要があります",
    twResidentRegexp: '（最初の2桁は大文字,最後の8桁は数字,2桁目は性別を表します【A,C,E男性,B,D,F女性】）',
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~core~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    expandMessage: "詳細",
    closeMessage: "たたむ",
    allMessage: "全て",
    customMessage: "カスタマイズ",
    expandRowNumMessage: "默认展开条件数'expandRow'必须为大于0的整数",
    filterMoreNotSupportMessage: "查询插件fiterMore不支持“{0}”方法",
    confirmMessage: "確認",
    multiselectMessage: "複数選択",
    singleSelectMessage: "単一選択",
    Selected: "選択済み",
    UnSelected: "選択されていません",
    AddAll: "全て追加する",
    Add: "追加",
    RemoveAll: "すべて削除する",
    Remove: "削除する",
    Data: {
        All: "オール",
    },
    All: "全て",
    Common: "一般的に使用されている",
    // DataTable
    DataTable: {
        SelectAll: "マルチ セレクション",
        SingleSelect: "シングル セレクション",
        NoResult: "そこに記録はない"
    },
    // AdvCascade
    PropertyError: 'Propertyバインディングの値が間違っています',
    // Pagination
    DropDownInfo: "どのページ{0}",
    Search: "検索",
    FirstPage: "トップ",
    PreviousPage: "前へ",
    NextPage: "次へ",
    LastPage: "最後",
    Pages: "ページ",
    Total: "合計",
    Items: "アイテム",

    // Input
    // Select
    BlankOption: "選択してください",

    // DateRangePicker
    DateRangePicker: {
        ApplyLabel: "確認",
        CancelLabel: "キャンセル",
        FromLabel: "スタート",
        ToLabel: "終了",
        CustomRangeLabel: "カスタマ",
        times: ["時間", "分", "秒"],
        timeChoose: '時間選択',
        BackDate: "期日に戻る",
        Clear: "クリア",
        Today: "閉じる",
        Close: "Close",
        DaysOfWeek: ["日", "月", "火", "水", "木", "金", "土"],
        MonthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
    },

    // TwoText
    TwoText: {
        dialogTitle: "検索コード表",
        tableHeaderTitle: "検索コード表",
        keyColumn: "キー",
        valueColumn: "値",
        confirmButton: "確認します",
        cancelButton: "キャンセル",
        error: "レコードを選択してください",
    },

    // Data
    // FullCalendar
    FullCalendar: {
        MonthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        MonthNamesShort: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
        DayNames: ["日", "月", "火", "水", "木", "金", "土"],
        DayNamesShort: ["日", "月", "火", "水", "木", "金", "土"],
        Today: ["今日"],
        FirstDay: 1,
        AM: "午前",
        PM: "午後",
        ButtonText: {
            //Prev: "<",
            //Next: ">",
            Prev: "先月",
            Next: "翌月",
            Today: "今日",
            Month: "月",
            Week: "週",
            Day: "日"
        }
    },

    // Integration
    // DropZone
    DropZone: {
        dictDefaultMessage: "ファイル削除及びアップロード",
        dictFallbackMessage: "このBrowserがdrag'n'drop fileアップロードをサポートしません。",
        dictFallbackText: "以前通りFallbackで該当ファイルをアップロードしてください。",
        dictInvalidFileType: "このファイルタイプがアップロードできない,ファイルタイプがサポートされていない。",
        dictFileTooBig: "ファイルサイズが大きすぎです。アップロードファイルサイズの最大限が{{maxFilesize}}MB。",
        dictResponseError: "ファイルアップロード失敗",
        dictCancelUpload: "ファイルキャンセル",
        dictCancelUploadConfirmation: "本当に今回のアップロードをキャンセルしますか。",
        dictRemoveFile: "ファイル移出",
        dictMaxFilesExceeded: "最大サイズのファイルが一つでもアップロードできます。",
    },

    // FileUpload
    UploadSuccess: 'アップロード成功！',
    RemoveSuccess: '成功を削除しました！',

    MSG_NOT_FOUND: "MSG NOT FOUND",
    MSG_REGULAR_EXPRESSION_ERROR: "数字しか入力できない",
    MSG_DATACONTEXT_KEY_DUPLICATE: "(DataContext key)重複キー値",

    //validator message
    InputValidator: {
        RequiredMessage: "必須項目を入力してください",
        LengthMessage: "{0}値から,より多くでよりよい{1}よりもなければ",
        MinLengthMessage: "値が少ないからである{0}よりもなければ",
        MaxLengthMessage: "値が{1}よりも少ないからでなければなりません",
    },
    DigitValidator: {
        ValueMessage: "{0}値から,より多くでよりよい{1}よりもなければ",
        MinValueMessage: "値よりも{0}できなければ",
        MaxValueMessage: "値が少ないからである{0}よりもなければ",
    },
    EmailValidator: "入力された有効な電子メール・アドレスでない",
    Page: {
        needContinue: "続けます?",
        messageFront: "終了後",
        messageEnd: "秒.",
        confirm: "確認します"
    },
    Required: "必須",
    DateTimePicker: {
        maxDateErrorMessage: "入力日が最大日を超える:",
        minDateErrorMessage: "入力日は最小の日になります:"
    },
    // UISearch
    Searching: '検索中...',
    //error message
    EffectiveDateError: '有効な日付を入力してください。',
    DateFormat: '正しい日付形式を入力してください',

    //TimeLine
    Online: 'オンライン',
    Offline: 'オフライン',

    // UILink
    noLinkTitle: 'ノータイトル',

    // popConfirm 
    PopConfirmTitle: '確かですか。',

    //sideMenu
    Overview: 'すべてのメニュー',
    MyFavorites: '私のコレクション',
    MenuNoResults: 'クエリ結果が見つかりません',
    MenuRecently: '最近:',
    SearchPlaceHolder: '検索メニュー'
};
