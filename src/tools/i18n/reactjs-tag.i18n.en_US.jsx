﻿module.exports = {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~creditcard~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    and: "—",
    month: 'Month',
    year:'Year',
    backNo:'Last Three',
    creditCard: "Credit Card",
    effectiveDate: "Effective Date",
    regexp: 'The first code is 2 or 3 or 4 or 5',
    regexp1: 'When the first code is 3, the second code can only be 5 or 7',
    regexp2: 'Length is 15',
    regexp3: 'Length is 16',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~cropper-upload~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    cropperCustomMessage: "Custom",
    imgTooLarge: "The picture is too large, please select again!",
    imgNotMatch: "The picture does not match, please select again!",
    formatNotMatch: "Sorry, the input format does not match.",
    proportionSelection: "Proportion selection",
    enterCustomScale: "Enter custom scale",
    reUpload: "Re-Upload",
    startCutting: "Start cutting",
    usePhotos: "Use photos",
    preview:"Preview",   

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~datetimepicker~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    formatSplitBothIsEmpty: 'The propType formatSplit or formatDateSplit is empty.',
    formatSplitIsEmpty: 'The propType formatSplit is empty.',
    formatMsg:'Please enter the correct date format',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-address~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    searchError: 'No relevant data.',
    addressLabelOne: 'Post Code',
    addressLabelTwo: 'City',
    addressLabelThree : 'Address',
    addressLabelFour: 'AddressKana',
    search: 'Search',
    reset: 'Reset',
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-finance~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    labelOne: 'Bank Code',
    labelTwo: 'Branch Name',
    labelThree : 'Post Branch Code',
    labelFour: 'Deposits',
    labelFive: 'Bank Account JP',
    labelSix: 'Post Account',
    labelSeven: 'Account Name',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-name~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    JPInputValidator: {
        jpNameRequiredMessage: "The field is required.",
        jpNameLengthMessage: "The value must be more than {0} and less than {1} characters long",
        jpNameMinLengthMessage: "The value must be more than {0} characters long",
        jpNameMaxLengthMessage: "The value must be less than {0} characters long",
        jpNameWholeFullTextMessage: 'Please enter in double-byte characters',
        jpNameExistsFullSpaceMessage: 'Separate the surname and first name with a double-byte space.'
    },

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-name-kanji~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    jpNameKanjiRequiredMessage: '',
    wholeFullTextMessage: '',
    existsFullSpaceMessage: '',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-name-kana~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tw-fixed-phone~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    fixedPhone: 'Fixed Phone',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tw-individual-id~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    idCard: 'ID card',
    twIndividualStringLength: "Field length must be 10 digits",
    twIndividualRegexp: '(The first letter is uppercase, the rest are numbers)',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tw-resident-permit~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    residentPermit: 'Residence permit',
    stringLength: "Field length must be 10 digits",
    twResidentRegexp: '(The first 2 digits are uppercase letters, the last 8 digits are numbers, and the second digit letter represents gender 【A, C, E male, B, D, F female】)',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~core~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    expandMessage: "More",
    closeMessage: "Fold",
    allMessage: "Unlimited",
    customMessage: "Custom",
    expandRowNumMessage: "The default value of expand row must greater than zero",
    filterMoreNotSupportMessage: "The plugin of Fiter More  does not support the function of {0}",
    confirmMessage: "Confirm",
    multiselectMessage: "Multiselect",
    singleSelectMessage: "Radio",
    Selected: "Selected",
    UnSelected: "UnSelected",
    AddAll: "Add All",
    Add: "Add",
    RemoveAll: "Remove All",
    Remove: "Remove",
    Data: {
        All: "All",
    },
    All: "All",
    Common: "Common",
    // DataTable
    DataTable: {
        SelectAll: "All",
        SingleSelect: "Single",
        NoResult: "There is no record."
    },

    // AdvCascade
    PropertyError: 'Wrong value for property binding',

    // Pagination
    DropDownInfo: "Each page {0}",
    Search: "Search",
    FirstPage: "First",
    PreviousPage: "Previous",
    NextPage: "Next",
    LastPage: "Last",
    Pages: "P",
    Total: "Total",
    Items: "Items",

    // Input
    // Select
    BlankOption: "Please Select",

    // DateRangePicker
    DateRangePicker: {
        ApplyLabel: "Confirm",
        CancelLabel: "Cancel",
        FromLabel: "Start",
        ToLabel: "End",
        CustomRangeLabel: "Custom",
        times: ["Hour", "Minutes", "Seconds"],
        timeChoose: 'Select Time',
        BackDate: "Select Date",
        Clear: "Clear",
        Today: "Now",
        Close: "Close",
        DaysOfWeek: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        MonthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    },

    // TwoText
    TwoText: {
        dialogTitle: "Search Code Table",
        tableHeaderTitle: "Search Code Table",
        keyColumn: "Key",
        valueColumn: "Value",
        confirmButton: "Confirm",
        cancelButton: "Cancel",
        error: "Please select a record",
    },

    // Data
    // FullCalendar
    FullCalendar: {
        MonthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        MonthNamesShort: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
        DayNames: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        DayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        Today: ["Today"],
        FirstDay: 1,
        AM: "am",
        PM: "pm",
        ButtonText: {
            //Prev: "<",
            //Next: ">",
            Prev: "last month",
            Next: "next month",
            Today: "today",
            Month: "month",
            Week: "week",
            Day: "day"
        }
    },

    // Integration
    // DropZone
    DropZone: {
        dictDefaultMessage: "Drop files or click here to upload",
        dictFallbackMessage: "Your browser does not support drag'n'drop file uploads.",
        dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
        dictInvalidFileType: "You cannot upload the file type, file type is not supported.",
        dictFileTooBig: "The file is too large ({{filesize}}MB). The maximum upload file support: {{maxFilesize}}MB.",
        dictResponseError: "Failed to upload file!",
        dictCancelUpload: "Cancel File",
        dictCancelUploadConfirmation: "Are you sure you want to cancel the upload?",
        dictRemoveFile: "Remove File",
        dictMaxFilesExceeded: "You can upload only a maximum of {{maxFiles}} files",
    },
    // FileUpload
    UploadSuccess: 'Upload Success!',
    RemoveSuccess: 'Removed Success!',

    MSG_NOT_FOUND: "MSG NOT FOUND",
    MSG_REGULAR_EXPRESSION_ERROR: "You can only enter Numbers",
    MSG_DATACONTEXT_KEY_DUPLICATE: "(DataContext key) is duplicate, please check.",

    //validator message
    InputValidator: {
        RequiredMessage: "The field is required.",
        LengthMessage: "The value must be more than {0} and less than {1} characters long",
        MinLengthMessage: "The value must be more than {0} characters long",
        MaxLengthMessage: "The value must be less than {0} characters long",
    },
    DigitValidator: {
        ValueMessage: "The value must be more than {0} and less than {1}",
        MinValueMessage: "The value must be more than {0}",
        MaxValueMessage: "The value must be less than {0}",
    },
    EmailValidator: "The input is not a valid email address",
    Page: {
        needContinue: "Need to continue?",
        messageFront: "Logout after ",
        messageEnd: " seconds.",
        confirm: "Confirm"
    },
    Required: "reuqired.",
    DateTimePicker: {
        maxDateErrorMessage: "The input date exceeds the maximum date:",
        minDateErrorMessage: "The input date exceeds the minimum date:"
    },
    // UISearch
    Searching: 'Searching...',
    //error message
    EffectiveDateError: 'Please enter Effective date.',
    DateFormat: "Please enter the correct date format.",

    //TimeLine
    Online: 'Online',
    Offline: 'Offline',

    // UILink
    noLinkTitle: 'No Title',

    // popConfirm 
    PopConfirmTitle: 'Are you sure?',

    //sideMenu
    Overview: 'Overview',
    MyFavorites: 'My Favorites',
    MenuNoResults: 'no results found for query',
    MenuRecently: 'Recently:',
    SearchPlaceHolder: 'Search menu'

};
