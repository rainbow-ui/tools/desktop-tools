import config from 'config';
const projectConfig = JSON.parse(sessionStorage.getItem('project_config')) || {};
let systemI18N = sessionStorage.getItem(config.DEFAULT_LOCALSTORAGE_I18NKEY) || localStorage.getItem(config.DEFAULT_LOCALSTORAGE_I18NKEY);
if (!systemI18N && projectConfig.telentConfig && projectConfig.telentConfig.defaultLang) {
	systemI18N = projectConfig.telentConfig.defaultLang;
	sessionStorage.setItem(config.DEFAULT_LOCALSTORAGE_I18NKEY, systemI18N);
} else if (!systemI18N && config.DEFAULT_SYSTEM_I18N) {
	systemI18N = config.DEFAULT_SYSTEM_I18N;
	sessionStorage.setItem(config.DEFAULT_LOCALSTORAGE_I18NKEY, systemI18N);
}
if (sessionStorage.getItem('changeconfiglang')) {
	systemI18N = sessionStorage.getItem('changeconfiglang');
}

try {
	module.exports = require("./reactjs-tag.i18n." + systemI18N);
} catch (exception) {
	module.exports = require("./reactjs-tag.i18n.en_US");
}