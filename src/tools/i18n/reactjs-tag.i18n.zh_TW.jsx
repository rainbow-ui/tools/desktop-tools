module.exports = {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~creditcard~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    and: "—",
    month: '月',
    year:'年(西)',
    backNo:'背面末三碼',
    creditCard: "信用卡號",
    effectiveDate: "有效日期",
    regexp: '第一碼為2或3或4或5',
    regexp1: '第一碼為3時,第二碼僅能為5或7',
    regexp2: '長度為15碼',
    regexp3: '長度為16碼',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~cropper-upload~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    cropperCustomMessage: '自定義',
    imgTooLarge: "圖片太大,請重新選擇!",
    imgNotMatch: "圖片不符合,請重新選擇!" ,
    formatNotMatch: "抱歉,輸入格式不匹配。",
    proportionSelection: "比例選擇",
    enterCustomScale: "輸入自定義比例",
    reUpload: "重新上傳",
    startCutting: "開始裁剪",
    usePhotos: "使用照片",
    preview:"預覽",

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~datetimepicker~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    formatSplitBothIsEmpty: 'propType formatSplit 或 formatDateSplit 為空。',
    formatSplitIsEmpty: 'propType formatSplit 為空。',
    formatMsg:'請輸入正確的日期格式',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-address~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    searchError: '沒有相關數據。',
    addressLabelOne: '郵政編碼',
    addressLabelTwo: '城市',
    addressLabelThree : '地址',
    addressLabelFour: '地址假名',
    search: '搜索',
    reset: '重置',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-finance~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    labelOne: '銀行代碼',
    labelTwo: '分支名稱',
    labelThree : '郵政分行代碼',
    labelFour: '存款',
    labelFive: '銀行賬戶 JP',
    labelSix: '郵政帳戶',
    labelSeven: '賬戶名',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-name~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    JPInputValidator:{
        jpNameRequiredMessage: "該字段是必需的。",
        jpNameLengthMessage: "該值的長度必須大於 {0} 且小於 {1} 個字符",
        jpNameMinLengthMessage: "該值的長度必須超過 {0} 個字符",
        jpNameMaxLengthMessage: "該值必須少於 {0} 個字符長",
        jpNameWholeFullTextMessage: '請輸入雙字節字符',
        jpNameExistsFullSpaceMessage: '用雙字節空格分隔姓氏和名字。'
    },

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-name-kanji~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    jpNameKanjiRequiredMessage: '',
    wholeFullTextMessage: '',
    existsFullSpaceMessage: '',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~jp-name-kana~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tw-fixed-phone~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    fixedPhone: '固定电话',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tw-individual-id~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    idCard: '身份证',
    twIndividualStringLength: "字段长度必须为10位",
    twIndividualRegexp: '（首字母大写,其余为数字）',
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~tw-resident-permit~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    residentPermit: '居住證',
    stringLength: "字段長度必須為10位",
    twResidentRegexp: '（前2位是大写字母,后8位是数字,并且第二碼字母代表性別【A、C、E男,B、D、F女】）',

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~core~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    expandMessage: '更多',
    closeMessage: '收起',
    allMessage: '不限',
    customMessage: '自定義',
    expandRowNumMessage: '默認展開條件數\'expandRow\'必須為大於0的整數',
    filterMoreNotSupportMessage: '查詢插件fiterMore不支持 "{0}"方法',
    confirmMessage: '確認',
    multiselectMessage: '多選',
    singleSelectMessage: '單選',
    Selected: '已選擇',
    UnSelected: '未選擇',
    AddAll: '添加全部',
    Add: '添加',
    RemoveAll: '移除全部',
    Remove: '移除',
    Data: {
        All: "所有",
    },
    All: "全部",
    Common: "常用",
    // DataTable
    DataTable: {
        SelectAll: "多選",
        SingleSelect: "單選",
    },

    // AdvCascade
    PropertyError: 'Property綁定的值錯誤',

    // Pagination
    DropDownInfo: "每頁{0}",
    Search: "搜索",
    FirstPage: "首頁",
    PreviousPage: "上一頁",
    NextPage: "下一頁",
    LastPage: "尾頁",
    Total: "總計",
    Items: "條",

    // Input
    // Select
    BlankOption: "請選擇",

    // DateRangePicker
    DateRangePicker: {
        ApplyLabel: '確定',
        CancelLabel: '取消',
        FromLabel: '起始時間',
        ToLabel: '結束時間',
        CustomRangeLabel: '自定義',
        times: ["小時", "分鐘", "秒"],
        timeChoose: '時間選擇',
        BackDate: "返回日期",
        Clear: "清空",
        Today: "現在",
        Close: "關閉",
        DaysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
        MonthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
    },

    // TwoText
    TwoText: {
        dialogTitle: "搜索Code Table",
        tableHeaderTitle: "搜索Code Table表格",
        keyColumn: "鍵",
        valueColumn: "值",
        confirmButton: "確認",
        cancelButton: "取消",
        error: "請選擇一條記錄",
    },

    // Data
    // FullCalendar
    FullCalendar: {
        MonthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        MonthNamesShort: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
        DayNames: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"],
        DayNamesShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],
        Today: ["今天"],
        FirstDay: 1,
        ButtonText: {
            //Prev: "<",
            //Next: ">",
            Prev: "上一月",
            Next: "下一月",
            Today: "本月",
            Month: "月",
            Week: "周",
            Day: "日"
        }
    },

    // Integration
    // DropZone
    DropZone: {
        dictDefaultMessage: "拖動檔案至該處(或點擊此處)",
        dictFallbackMessage: "瀏覽器不支持拖放檔案上傳",
        dictFallbackText: "請使用傳統方式上傳檔案",
        dictInvalidFileType: "不能上傳該類型檔案,檔案類型不支持。",
        dictFileTooBig: "檔案過大({{filesize}}MB). 上傳檔案最大支持: {{maxFilesize}}MB.",
        dictResponseError: "檔案上傳失敗",
        dictCancelUpload: "取消上傳",
        dictCancelUploadConfirmation: "確定要取消上傳嗎?",
        dictRemoveFile: "移除檔案",
        dictMaxFilesExceeded: "一次最多只能上傳{{maxFiles}}個檔案",
    },

    // FileUpload
    UploadSuccess: '上傳成功！',
    RemoveSuccess: '刪除成功！',

    MSG_NOT_FOUND: "資訊沒有找到",
    MSG_REGULAR_EXPRESSION_ERROR: "僅可錄入數字",
    MSG_DATACONTEXT_KEY_DUPLICATE: "(DataContext key)重復,請檢查.",

    //validator message
    InputValidator: {
        RequiredMessage: "欄位必填,不能為空",
        LengthMessage: "欄位值長度必須大於{0}個,小於{1}個",
        MinLengthMessage: "欄位值長度必須大於{0}個",
        MaxLengthMessage: "欄位值長度必須小於{0}個",
    },
    DigitValidator: {
        ValueMessage: "值必須大於{0},小於{1}",
        MinValueMessage: "值必須大於{0}",
        MaxValueMessage: "值必須小於{0}",
    },
    EmailValidator: "郵箱地址不合法",
    Page: {
        needContinue: "繼續操作?",
        messageFront: "登出後",
        messageEnd: "秒後退出.",
        confirm: "確認"
    },
    DateTimePicker: {
        maxDateErrorMessage: '輸入日期超過最大日期:',
        minDateErrorMessage: '輸入日期小於最早日期:'
    },
    // UISearch
    PleaseSelect: '搜寻中...',
    //error message
    EffectiveDateError: '請輸入有效的日期。',
    DateFormat: "請輸入正確的日期格式。",
    Required: "欄位必填.",

    //TimeLine
    Online: '在線',
    Offline: '離線',

    // UILink
    noLinkTitle: '無提示',

    // popConfirm 
    PopConfirmTitle: '你確定此項操作嗎？',

    //sideMenu
    Overview: '全部菜單',
    MyFavorites: '我的收藏',
    MenuNoResults: '未找到查詢結果',
    MenuRecently: '最近:',
    SearchPlaceHolder: '搜索菜單'
};
