import config from 'config';
import { SessionContext, LocalContext } from 'rainbow-desktop-cache';

module.exports = {

    getUserLang: function (langUrl) {
        let lang = sessionStorage.getItem(config.DEFAULT_LOCALSTORAGE_I18NKEY);
        if (!lang) {
            let authorization = sessionStorage.getItem('Authorization');
            if (authorization == null || authorization == undefined || authorization == '') {
                lang = this.getSystemI18N();
            } else {
                $.ajax({
                    method: 'GET',
                    url: langUrl,
                    async: false,
                    contentType: 'application/json;charset=UTF-8',
                    xhrFields: { withCredentials: true },
                    crossDomain: true,
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'Bearer ' + authorization.substr(13).split('&')[0]);
                    },
                    success: function (d, status, xhr) {
                        if (d) {
                            lang = d
                            // localStorage.setItem(config.DEFAULT_LOCALSTORAGE_I18NKEY, d);
                            sessionStorage.setItem(config.DEFAULT_LOCALSTORAGE_I18NKEY, d);
                        }
                        const env = sessionStorage.getItem('x-ebao-env');
                        const tenant = sessionStorage.getItem('x-ebao-response-tenant-code');
                        let henv = xhr.getResponseHeader('x-ebao-env')
                        let htenant = xhr.getResponseHeader('x-ebao-response-tenant-code')
                        if (!env && henv) {
                            sessionStorage.setItem('x-ebao-env', henv)
                        }
                        if (!tenant && htenant) {
                            sessionStorage.setItem('x-ebao-response-tenant-code', htenant);
                        }
                    },
                    error: function (error) {
                        if (error.status == 403 || error.status == 401) {
                            const cfg = SessionContext.get('project_config');
                            logout(cfg);
                        } else {
                            window.toastr ? toastr['error']('Get User I18n API Error.', 'ERROR') : null;
                        }
                    }
                });
            }
            return lang;
        } else {
            return lang;
        }
    },

    getI18nData: function (lang, i18nUrl) {
        let i18n_cache_data = null;
        let projectPathKey = window.buildKey(window.currentUrl);
        const i18n_cache_key = 'i18n_Cache_' + lang + '_' + projectPathKey;

        if (config.DEFAULT_I18N_CACHE) {
            i18n_cache_data = LocalContext.get(i18n_cache_key);
        }
        if (i18n_cache_data) {
            return i18n_cache_data;
        } else {
            let group = config.DEFAULT_I18N_CONFIGURATION_GROUP;
            if (!group instanceof Array) {
                group = [config.DEFAULT_I18N_CONFIGURATION_GROUP];
            }
            $.ajax({
                method: 'POST',
                url: i18nUrl + '?languageId=' + lang,
                async: false,
                contentType: 'application/json;charset=UTF-8',
                data: JSON.stringify(group),
                xhrFields: { withCredentials: true },
                crossDomain: true,
                beforeSend: function (xhr) {
                    let authorization = SessionContext.get('Authorization');
                    if (authorization == null || authorization == undefined || authorization == '') {
                    } else {
                        xhr.setRequestHeader('Authorization', 'Bearer ' + authorization.substr(13).split('&')[0]);
                    }
                },
                success: function (d) {
                    if (config.DEFAULT_I18N_CACHE) {
                        LocalContext.put(i18n_cache_key, d);
                    }

                    i18n_cache_data = d;
                },
                error: function (error) {
                    if (error.status == 403 || error.status == 401) {
                        const cfg = SessionContext.get('project_config');
                        logout(cfg);
                    } else {
                        window.toastr ? toastr['error']('I18n API Error.', 'ERROR') : null;
                    }
                }
            });
            return i18n_cache_data || {};
        }
    },

    getDiMoI18nData: function (lang, i18nUrl) {
        let i18n_cache_data = null;
        let projectPathKey = window.buildKey(window.currentUrl);
        const i18n_cache_key = 'i18n_Cache_' + lang + '_' + projectPathKey;

        if (config.DEFAULT_I18N_CACHE) {
            i18n_cache_data = LocalContext.get(i18n_cache_key);
        }
        if (i18n_cache_data) {
            return i18n_cache_data;
        } else {
            let group = config.DEFAULT_I18N_CONFIGURATION_GROUP;
            if (!group instanceof Array) {
                group = [config.DEFAULT_I18N_CONFIGURATION_GROUP];
            }
            let userInfo = SessionContext.get('userInfo')
            let tenantCode = userInfo ? userInfo.firmCode ? userInfo.firmCode : '' : ''
            let postData = {
                "categories": group,
                "language": lang.split('_')[0],
                "tenantCode": tenantCode
            }
            $.ajax({
                method: 'POST',
                url: i18nUrl,
                async: false,
                contentType: 'application/json;charset=UTF-8',
                data: JSON.stringify(postData),
                xhrFields: { withCredentials: true },
                crossDomain: true,
                beforeSend: function (xhr) {
                    let authorization = SessionContext.get('Authorization');
                    if (authorization == null || authorization == undefined || authorization == '') {
                    } else {
                        xhr.setRequestHeader('Authorization', 'Bearer ' + authorization.substr(13).split('&')[0]);
                    }
                },
                success: function (d) {

                    if (config.DEFAULT_I18N_CACHE) {
                        LocalContext.put(i18n_cache_key, d);
                    }

                    i18n_cache_data = d;
                },
                error: function (error) {
                    if (error.status == 403 || error.status == 401) {
                        const cfg = SessionContext.get('project_config');
                        logout(cfg);
                    } else {
                        window.toastr ? toastr['error']('I18n API Error.', 'ERROR') : null;
                    }
                }
            });
            return i18n_cache_data || {};
        }
    },

    getSystemI18N: function () {
        let systemI18N = sessionStorage.getItem(config.DEFAULT_LOCALSTORAGE_I18NKEY) || localStorage.getItem(config.DEFAULT_LOCALSTORAGE_I18NKEY);
        if (systemI18N) {
            return systemI18N;
        } else {
            this.setSystemI18N(config.DEFAULT_SYSTEM_I18N);
            return config.DEFAULT_SYSTEM_I18N;
        }
    },

    setSystemI18N: function (value) {
        sessionStorage.setItem(config.DEFAULT_LOCALSTORAGE_I18NKEY, value);
    },

    /**@ignore
     * I18N format message
     */
    format: function (message, ...args) {
        $.each(args, function (index, element) {
            message = message.replace("{" + index + "}", element);
        });

        return message;
    },
    /**@ignore
     * I18N format object message
     */
    formatObject: function (message, ...args) {
        let elementArray = [];
        $.each(args, function (index, element) {
            let msgArray = message.split("{" + index + "}");

            if (msgArray.length == 2) {
                elementArray.push(msgArray[0]);
                elementArray.push(element);
                message = msgArray[1];
            }
        });
        elementArray.push(message);
        return elementArray;
    },
    /**@ignore
     * Get i18n message
     */
    getMessage: function (message) {
        if (message == null || message == undefined) {
            return "MSG Not Found";//i18n.MSG_NOT_FOUND;
        }
        return message;
    }
}